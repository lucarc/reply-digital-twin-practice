from __future__ import print_function
from time import sleep
import paho.mqtt.client as mqtt
import glob
import serial

mqtt_broker_address = "localhost"

mqtt_topic = "arduino/data"

#############################################
arduino_port = "/dev/ttyACM0"
#############################################

device = glob.glob(arduino_port)[0]

# Open a connection through the serial port
try:
    arduino = serial.Serial(device, 4800, timeout=15)
except Exception as ex:
    template = "An exception of type {0} occurred. Arguments:\n{1!r}"
    message = template.format(type(ex).__name__, ex.args)
    print(message)
    print("Please check the port!")

# Create a new MQTT Client instance assigning a client ID
client = mqtt.Client("Producer_1")
print("Connecting to MQTT Broker")
client.connect(mqtt_broker_address, 1883)

# Start publishing messages
publish_rate = 1 # The same as the one set on the Arduino
while True:
    sample = str(arduino.readline())
    client.publish(mqtt_topic, sample)

    print("Publishing message to topic {} message:\n{}".format(mqtt_topic, sample))
    sleep(publish_rate)
