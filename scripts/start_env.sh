#!/usr/bin/env bash

mkdir -p /var/tmp/mqtt-kafka-docker
docker rm -f mqtt-kafka-docker

docker run -p 8082:8082 -p 3030:3030 -p 8161:8161 -p 61613:61613 -p 9092:9092 -e DISABLE_JMX=1 -e RUNTESTS=0 -e CONNECT_HEAP=1G --net=host -v /var/tmp/mqtt-kafka-docker:/tmp --name=mqtt-kafka-docker landoop/example-mqtt
