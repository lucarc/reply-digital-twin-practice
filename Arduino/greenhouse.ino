#include <SimpleDHT.h>

// for DHT11, 
//      VCC: 5V or 3V
//      GND: GND
//      DATA: 2
int pinDHT11 = 2;
SimpleDHT11 dht11;

// for FC-28, 
//      VCC: 5V or 3V
//      GND: GND
//      DATA: A1, A2
int sensor_pin_pot1 = A1;
int output_value_pot1;
int sensor_pin_pot2 = A2;
int output_value_pot2;

void setup() {
  Serial.begin(9600);
}

void loop() {
  
  // read with raw sample data.
  byte temperature = 0;
  byte humidity = 0;
  byte data[40] = {0};

  if (dht11.read(pinDHT11, &temperature, &humidity, data)) {
    //Serial.println("Read DHT11 failed");
    return;
  }
  
  output_value_pot1 = analogRead(sensor_pin_pot1);
  output_value_pot2 = analogRead(sensor_pin_pot2);

  // We map the analog output values from the soil moisture sensor FC-28 to 0-100,
  // because the moisture is measured in percentage.
  // When we took the readings from the dry soil the sensor value was XXX,
  // and from the wet soil the sensor value was YYY.
  // So, we mapped these values to get the moisture.
  
  // How to derive the values of XXX and YYY?

  output_value_pot1 = map(output_value_pot1,XXX,YYY,0,100);
  output_value_pot2 = map(output_value_pot2,XXX,YYY,0,100);

  Serial.print("{ ");
  
  Serial.print("\"humidity_air\":");
  Serial.print((int)humidity);
  
  Serial.print(",\"temperature_air\":");
  Serial.print((int)temperature);
  
  Serial.print(",\"moisture_pot_1\":");
  Serial.print(output_value_pot1);

  Serial.print(",\"moisture_pot_2\":");
  Serial.print(output_value_pot2);
  
  Serial.println(" }");

  // Greenhouse sampling rate is 1HZ.
  delay(1000);
}